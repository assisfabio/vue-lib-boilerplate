# Vue Lib Boilerplate

Boilerplate para desenvolvimento de bilbliotecas utilizando Vue, Vuetify e Storybook

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve:storybook
```

### Compiles and minifies for production

```
npm run build:lib
```

### Run your unit tests

```
npm run test:unit
```

### Run your end-to-end tests

```
npm run test:e2e
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
