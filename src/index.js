import { Button, FormataValor } from "./components";

const manifest = require("./../package.json");

export function install(Vue) {
  if (install.installed) return;
  install.installed = true;
  Vue.component(manifest.name, {
    Button,
    FormataValor,
  });
}

const plugin = {
  install,
};

let GlobalVue = null;
if (typeof window !== "undefined") {
  GlobalVue = window.Vue;
} else if (typeof global !== "undefined") {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

export { Button, FormataValor };
