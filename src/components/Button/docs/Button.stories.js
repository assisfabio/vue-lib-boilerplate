import { storiesOf } from "@storybook/vue";
import { withKnobs, text, boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import Button from "../Button.vue";

export default {
  title: "Storybook W Knobs",
  decorators: [withKnobs],
};

// Assign `props` to the story's component, calling
// knob methods within the `default` property of each prop,
// then pass the story's prop data to the component’s prop in
// the template with `v-bind:` or by placing the prop within
// the component’s slot.
export const exampleWithKnobs = () => ({
  components: { Button },
  props: {
    disabled: {
      default: boolean("Disabled", false),
    },
    text: {
      default: text("Text", "Label do Botão"),
    },
  },
  methods: {
    log: action("log"),
  },
  template: `<Button :disabled="disabled" :on-click="log">{{ text }}</Button>`,
});
