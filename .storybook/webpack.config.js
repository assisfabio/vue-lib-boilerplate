const path = require("path");

module.exports = async ({ config }) => {
  config.resolve.alias["~storybook"] = path.resolve(__dirname);

  config.module.rules.push({
    resourceQuery: /blockType=story/,
    loader: "vue-storybook",
  });

  config.module.rules.push({
    test: /\.s(a|c)ss$/,
    use: ["style-loader", "css-loader", "sass-loader"],
    include: path.resolve(__dirname, "../"),
  });

  // Extend it as you need.
  function resolve(dir) {
    return path.join(__dirname, "..", dir);
  }

  config.resolve = {
    extensions: [".js", ".vue", ".json"],
    alias: {
      vue$: "vue/dist/vue.esm.js",
      "@": resolve("src"),
    },
  };

  return config;
};
